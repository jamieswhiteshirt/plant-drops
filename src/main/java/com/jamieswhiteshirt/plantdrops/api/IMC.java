package com.jamieswhiteshirt.plantdrops.api;

public final class IMC {
    /**
     * Key for a ResourceLocation message to add a harvester item.
     */
    public static final String HARVESTER_ITEM_ADD = "harvesterItem:add";

    /**
     * Key for a ResourceLocation message to add a plant block.
     */
    public static final String PLANT_BLOCK_ADD = "plantBlock:add";
}
