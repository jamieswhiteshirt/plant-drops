package com.jamieswhiteshirt.plantdrops;

import com.jamieswhiteshirt.plantdrops.api.IMC;
import com.jamieswhiteshirt.plantdrops.common.CommonProxy;
import com.jamieswhiteshirt.plantdrops.common.PlantDropsEventHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import org.apache.logging.log4j.Logger;

@Mod(
        modid = PlantDrops.MODID,
        version = PlantDrops.VERSION,
        name = PlantDrops.NAME,
        guiFactory = "com.jamieswhiteshirt.plantdrops.client.PlantDropsGuiFactory"
)
public class PlantDrops {
    public static final String MODID = "plantdrops";
    public static final String VERSION = "1.0";
    public static final String NAME = "Plant Drops";

    @SidedProxy(
            clientSide = "com.jamieswhiteshirt.plantdrops.client.ClientProxy",
            serverSide = "com.jamieswhiteshirt.plantdrops.client.CommonProxy",
            modId = MODID
    )
    public static CommonProxy proxy;
    public static Logger logger;
    public static PlantDropsEventHandler dropsEventHandler;

    public static final Item CLIPPERS = new Item().setMaxStackSize(1).setUnlocalizedName(MODID + ".clippers").setCreativeTab(CreativeTabs.TOOLS);


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        GameRegistry.register(CLIPPERS, new ResourceLocation(MODID, "clippers"));
        proxy.registerRenderers();
        MinecraftForge.EVENT_BUS.register(dropsEventHandler = new PlantDropsEventHandler(new Configuration(event.getSuggestedConfigurationFile())));
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        GameRegistry.addRecipe(new ShapedOreRecipe(CLIPPERS, "SF", " S", 'S', "stickWood", 'F', Items.FLINT));
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        dropsEventHandler.validate();
    }

    @EventHandler
    public void onIMC(FMLInterModComms.IMCEvent event) {
        for (FMLInterModComms.IMCMessage message : event.getMessages()) {
            if (message.isResourceLocationMessage()) {
                ResourceLocation resourceLocation = message.getResourceLocationValue();
                if (IMC.HARVESTER_ITEM_ADD.equals(message.key)) {
                    dropsEventHandler.registerHarvesterItem(resourceLocation);
                } else if (IMC.PLANT_BLOCK_ADD.equals(message.key)) {
                    dropsEventHandler.registerPlantBlock(resourceLocation);
                }
            }
        }
    }
}
