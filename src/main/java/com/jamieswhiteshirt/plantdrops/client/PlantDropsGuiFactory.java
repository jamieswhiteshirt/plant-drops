package com.jamieswhiteshirt.plantdrops.client;

import com.jamieswhiteshirt.plantdrops.PlantDrops;
import com.jamieswhiteshirt.plantdrops.common.PlantDropsEventHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.Set;

@SideOnly(Side.CLIENT)
public class PlantDropsGuiFactory implements IModGuiFactory {
    @Override
    public void initialize(Minecraft minecraftInstance) {}

    @Override
    public Class<? extends GuiScreen> mainConfigGuiClass() {
        return PlantDropsConfigGui.class;
    }

    @Override
    public Set<RuntimeOptionCategoryElement> runtimeGuiCategories() {
        return null;
    }

    @Nullable
    @Override
    public RuntimeOptionGuiHandler getHandlerFor(RuntimeOptionCategoryElement element) {
        return null;
    }

    public static class PlantDropsConfigGui extends GuiConfig {
        public PlantDropsConfigGui(GuiScreen parent) {
            super(
                    parent,
                    (new ConfigElement(PlantDrops.dropsEventHandler.config.getCategory("general"))).getChildElements(),
                    PlantDrops.MODID, PlantDropsEventHandler.CONFIG_ID, false, false,
                    GuiConfig.getAbridgedConfigPath(PlantDrops.dropsEventHandler.config.toString())
            );
        }
    }
}
