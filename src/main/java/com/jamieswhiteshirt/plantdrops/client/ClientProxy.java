package com.jamieswhiteshirt.plantdrops.client;

import com.jamieswhiteshirt.plantdrops.PlantDrops;
import com.jamieswhiteshirt.plantdrops.common.CommonProxy;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy {
    @Override
    public void registerRenderers() {
        ModelLoader.setCustomModelResourceLocation(PlantDrops.CLIPPERS, 0, new ModelResourceLocation(new ResourceLocation(PlantDrops.MODID, "clippers"), "inventory"));
    }
}
