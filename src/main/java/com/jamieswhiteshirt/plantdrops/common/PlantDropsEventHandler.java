package com.jamieswhiteshirt.plantdrops.common;

import com.jamieswhiteshirt.plantdrops.PlantDrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.IForgeRegistry;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class PlantDropsEventHandler {
    public static final String CONFIG_ID = "plantDrops";
    public final Configuration config;
    private final Set<ResourceLocation> defaultHarvesterItems = new HashSet<ResourceLocation>();
    private Set<ResourceLocation> allHarvesterItems;
    private final Set<ResourceLocation> defaultPlantBlocks = new HashSet<ResourceLocation>();
    private Set<ResourceLocation> allPlantBlocks;

    public PlantDropsEventHandler(Configuration config) {
        this.config = config;
        buildSets();
    }

    private void buildSets() {
        Property propHarvesterItems = config.get(
                "general",
                "harvesterItems",
                new String[] {
                        "minecraft:shears",
                        "plantdrops:clippers"
                },
                "Item IDs of harvester items that will allow plant blocks to drop items"
        );
        propHarvesterItems.setLanguageKey("plantdrops.configgui.harvesterItems");
        Property propPlantBlocks = config.get(
                "general",
                "plantBlocks",
                new String[] {
                        "minecraft:tall_grass",
                        "minecraft:yellow_flower",
                        "minecraft:red_flower",
                        "minecraft:sapling"
                },
                "Block IDs of plant blocks that will require harvester items to drop items"
        );
        propPlantBlocks.setLanguageKey("plantdrops.configgui.plantBlocks");
        config.save();
        allHarvesterItems = createResourceLocationSet(defaultHarvesterItems, propHarvesterItems.getStringList());
        allPlantBlocks = createResourceLocationSet(defaultPlantBlocks, propPlantBlocks.getStringList());
    }

    private Set<ResourceLocation> createResourceLocationSet(Set<ResourceLocation> defaultSet, String[] strings) {
        Set<ResourceLocation> result = new HashSet<ResourceLocation>();
        for (String string : strings) {
            result.add(new ResourceLocation(string));
        }
        result.addAll(defaultSet);
        return result;
    }

    private void validateResourceLocationSet(String type, Set<ResourceLocation> set, IForgeRegistry registry) {
        for (ResourceLocation value : set) {
            if (!registry.containsKey(value)) {
                PlantDrops.logger.warn("(" + PlantDrops.NAME + ") Missing " + type + " \"" + value + "\"");
            }
        }
    }

    public void validate() {
        validateResourceLocationSet("harvester item ID", allHarvesterItems, ForgeRegistries.ITEMS);
        validateResourceLocationSet("plant block ID", allPlantBlocks, ForgeRegistries.BLOCKS);
    }

    public void registerHarvesterItem(ResourceLocation resourceLocation) {
        defaultHarvesterItems.add(resourceLocation);
        allHarvesterItems.add(resourceLocation);
    }

    public void registerPlantBlock(ResourceLocation resourceLocation) {
        defaultPlantBlocks.add(resourceLocation);
        allPlantBlocks.add(resourceLocation);
    }

    private boolean canHarvest(IBlockState state, @Nullable EntityPlayer harvester) {
        if (allPlantBlocks.contains(state.getBlock().getRegistryName())) {
            if (harvester != null) {
                return allHarvesterItems.contains(harvester.getHeldItemMainhand().getItem().getRegistryName());
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onHarvestDrops(BlockEvent.HarvestDropsEvent event) {
        if (!canHarvest(event.getState(), event.getHarvester())) {
            //event.getDrops().clear();
            event.setDropChance(0.0F);
        }
    }

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (PlantDrops.MODID.equals(event.getModID())) {
            if (CONFIG_ID.equals(event.getConfigID())) {
                buildSets();
                validate();
            }
        }
    }
}
